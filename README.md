# UMAP23_KARSCombiningGNNsEncSentenceEnc
Fork of the source code for the paper titled *Combining GNNs and Sentence Encoders for Knowledge-aware Recommendations*.

You will find the two datasets used in this work (movielens and yt_100_most_subscribed_fr), and also the scripts:
1. to learn graph embeddings
2. to learn word embeddings
3. to train the deep model and get the predictions.

In addition, we added the pre-trained embeddings used for this work (dict saved as `.pickle` files), to skip the embedding learning step.
Source code is documented, so to make it easier to run it.

The deep model requires 19 GB of (GPU) RAM concerning movielens dataset. To leverage GPU performances, I recommend using *NVIDIA GPU Cloud* OS (available at [OVH](https://www.ovhcloud.com/en/public-cloud/nvidia-gpu-cloud/)) and the [TensorFlow Docker container](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorflow).
