#!/usr/bin/python3

import csv

groundTruth = {}

correctLikes = 0
likes = 0
correctDislikes = 0
dislikes = 0

with open('movielens/split/0/test.tsv') as tsvFile:
    rows = csv.reader(tsvFile, delimiter='\t')
    for row in rows:
        user, item, rating = [int(field) for field in row]
        groundTruth[tuple([user, item])] = rating

with open('cross_validations/0/0 feature based/top5_predictions.tsv') as tsvfile:
    reader = csv.reader(tsvfile, delimiter='\t')
    for userPredictions in reader:
        rows = userPredictions[0].split('\n')
        for row in rows[1:]:
            user, item, score = [field for field in row.split(' ') if field != ''][1:]
            user, item, score = int(user), int(item), float(score)
            prediction = round(score)
            truthPrediction = groundTruth[tuple([user, item])]
            if prediction == truthPrediction:
                if prediction == 1:
                    correctLikes += 1
                else:
                    correctDislikes += 1
            if truthPrediction == 1:
                likes += 1
            else:
                dislikes += 1

print(f'{correctLikes=}/{likes} = {round(correctLikes/likes, 5)}')
print(f'{correctDislikes=}/{dislikes} = {round(correctDislikes/dislikes, 5)}')
correctAll = correctLikes + correctDislikes
all_ = likes + dislikes
print(f'{correctAll=}/{all_} = {round(correctAll/all_, 5)}')
