#!/usr/bin/python3

import os
import re
import json
import csv
import tensorflow as tf
import tensorflow_ranking as tfr

dataset = 'yt_100_most_subscribed_fr'
path = '../datasets/yt_100_most_subscribed_fr/first_try/cross_validations/'

os.chdir(path)

top = 5
ndcg = tfr.keras.metrics.NDCGMetric()#topn=top)
map_ = tfr.keras.metrics.MeanAveragePrecisionMetric()#topn=top)
mrr = tfr.keras.metrics.MRRMetric()#topn=top)

# Can compile regex to make their use faster.
modelRegex = '\d+ ((?:single_feature (?:0|2))|(?:(?:entity|feature) based))'
statisticsRegex = '([a-z]+): ((?:0|1)\.\d+)'

specific = None

models = {
    'single_feature 0': 'graph',
    'single_feature 2': 'word',
    'feature based': 'feature',
    'entity based': 'entity',
}

models = {model: f'{models[model]} {"(" + specific + ")" if specific is not None else ""}' for model in models}

def getAverage(l):
    return sum(l) / len(l)

for directoryIndex, directory in enumerate(os.listdir()):
    with open(f'{directory}/logs.txt') as f:
        lines = f.read().splitlines()
    model = None
    print('directoryIndex', directoryIndex)
    if directoryIndex == 0:
        initialInterestingStatistics = []
        for line in lines:
            lineStatisticsMatches = re.findall(statisticsRegex, line)
            if lineStatisticsMatches != []:
                initialInterestingStatistics = [key for key, _ in lineStatisticsMatches]
                break

        initialInterestingStatistics.remove('loss')
        print(initialInterestingStatistics)
        interestingStatistics = initialInterestingStatistics + ['mrr', 'map', 'ndcg']

        modelsStatistics = {
            model: {
                statistic: [] for statistic in interestingStatistics
            } for model in models.values()
        }

    for line, nextLine in zip(lines, lines[1:]):
        lineModelMatch = re.match(modelRegex, line)
        if lineModelMatch:
            model = lineModelMatch.group(1)
            model = models[model]
            print(nextLine)
        if nextLine == 'Loading embeddings to be fitted/tested...' and model is not None:
            statistics = {key: float(value) for key, value in re.findall(statisticsRegex, line)}
            for statistic in initialInterestingStatistics:
                modelsStatistics[model][statistic] += [statistics[statistic]]

    continue

    usersScores = {}

    with open(f'../{dataset}/split/{directory}/test.tsv') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        userScores = {}
        for row in reader:
            user, item, score = [int(field) for field in row]
            userScores[item] = score
            if not user in usersScores:
                usersScores[user] = {}
            usersScores[user][item] = score

    evaluated_ndcgs = []
    evaluated_mrrs = []
    evaluated_maps = []

    for modelDirectory in os.listdir(directory):
        if modelDirectory != 'logs.txt':
            print(modelDirectory)
            model = ' '.join(modelDirectory.split()[1:])
            with open(f'{directory}/{modelDirectory}/top5_predictions.tsv') as tsvfile:
                reader = csv.reader(tsvfile, delimiter='\t')
                usersPredictions = list(reader)
                for userPredictionsIndex, userPredictions in enumerate(usersPredictions):
                    if userPredictionsIndex % 100 == 0:
                        print(userPredictionsIndex, len(usersPredictions))
                    rows = userPredictions[0].split('\n')
                    userPredictions = {}
                    for row in rows[1:]:
                        user, item, score = [field for field in row.split(' ') if field != ''][1:]
                        user, item, score = int(user), int(item), float(score)
                        userPredictions[item] = score
                    userScores = usersScores[user]
                    y_true = [[userScores[item] for item in userPredictions]]
                    y_pred = [[userPredictions[item] for item in userPredictions]]
                    #y_true = tf.convert_to_tensor(y_true)
                    #y_pred = tf.convert_to_tensor(y_pred)
                    #print(y_true)
                    #print(y_pred)
                    #print(type(y_true))
                    #print(ndcg(y_true, y_pred))
                    evaluated_ndcgs += [ndcg(y_true, y_pred).numpy()]
                    evaluated_mrrs += [mrr(y_true, y_pred).numpy()]
                    evaluated_maps += [map_(y_true, y_pred).numpy()]

            for statistic, evaluated_l in [['ndcg', evaluated_ndcgs], ['mrr', evaluated_mrrs], ['map', evaluated_maps]]:
                average = getAverage(evaluated_l)
                modelsStatistics[models[model]][statistic] += [average]

print(json.dumps(modelsStatistics, indent = 4))
