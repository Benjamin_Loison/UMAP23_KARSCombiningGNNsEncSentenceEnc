#!/usr/bin/python3

import matplotlib.pyplot as plt

'''
    "graph": {
        "precision": [
            0.7974,
            0.7966,
            0.7960,
            0.7973,
            0.7958,
        ],
        "recall": [
            0.7494,
            0.7495,
            0.7497,
            0.7485,
            0.7502,
        ],
    },
'''

configurations = {
    "word": {
        "precision": [
            0.9871,
            0.9811,
            0.9840,
            0.9850,
            0.9833,
        ],
        "recall": [
            0.9921,
            0.9892,
            0.9905,
            0.9910,
            0.9904,
        ],
    },
    "feature": {
        "precision": [
            0.9978,
            0.9973,
            0.9977,
            0.9977,
            0.9972,
        ],
        "recall": [
            0.9982,
            0.9978,
            0.9980,
            0.9980,
            0.9976,
        ],
    },
    "entity": {
        "precision": [
            0.9705,
            0.9655,
            0.9734,
            0.9648,
            # Do not have this value, the program stopped for an unknown reason.
        ],
        "recall": [
            0.9824,
            0.9793,
            0.9841,
            0.9780,
        ],
    },
}

def getF1Score(precision, recall):
    return 2 * (precision * recall) / (precision + recall)

for configurationName in configurations:
    configuration = configurations[configurationName]
    configurations[configurationName]['F1 score'] = [getF1Score(precision, recall) for precision, recall in zip(configuration['precision'], configuration['recall'])]

toDelete = []#['precision', 'recall', 'F1 score']#['mrr', 'map', 'ndcg']
for configuration in configurations:
   # print(configuration)
    for metric in toDelete:
        #print(metric)
        #print(configurations[configuration])
        #break
        del configurations[configuration][metric]
    #break

metrics = list(configurations.values())[0].keys()

# Single figure?
for metricIndex, metric in enumerate(metrics):
    # Make 5 being automatically detected.
    plt.subplot(len(configurations[list(configurations.keys())[0]]), 1, metricIndex + 1)
    plt.title(metric)
    plt.xticks(rotation=90)
    plt.boxplot([configuration[metric] for configuration in configurations.values()], labels = configurations.keys())
    #plt.show()
    plt.plot()
    #break

plt.tight_layout()
#plt.show()
plt.savefig('statistics.svg')
