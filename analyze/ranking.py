import tensorflow_ranking as tfr

y_true = [[1., 1., 0., 1., 0.]]
#y_pred = [[3., 1., 2.]]
y_pred = [[0.972680, 0.933068, 0.912088, 0.894216, 0.879251]]
top = 5
ndcg = tfr.keras.metrics.NDCGMetric(topn=top)
dcg = tfr.keras.metrics.DCGMetric(topn=top)
map_ = tfr.keras.metrics.MeanAveragePrecisionMetric(topn=top)
mrr = tfr.keras.metrics.MRRMetric(topn=top)
evaluated_ndcg = ndcg(y_true, y_pred).numpy()
evaluated_dcg = dcg(y_true, y_pred).numpy()
#print(evaluated_dcg / evaluated_ndcg)
print(evaluated_ndcg)

##

import os
import csv

path = '/home/benjamin/Desktop/bens_folder/school/ens/asp/aria/project/code/UMAP23_KARSCombiningGNNsEncSentenceEnc/movielens/'

os.chdir(path)

usersScores = {}

with open('test.tsv') as tsvfile:
    reader = csv.reader(tsvfile, delimiter='\t')
    userScores = {}
    for row in reader:
        #print(row)
        user, item, score = [int(field) for field in row]
        #print(user, item, score)
        userScores[item] = score
        if not user in usersScores:
            usersScores[user] = {}
        usersScores[user][item] = score

mrrs = []
evaluated_ndcgs = []
evaluated_mrrs = []
evaluated_maps = []
with open('top5_predictions.tsv') as tsvfile:
    reader = csv.reader(tsvfile, delimiter='\t')
    usersPredictions = list(reader)
    for userPredictionsIndex, userPredictions in enumerate(usersPredictions):
        if userPredictionsIndex % 100 == 0:
            print(userPredictionsIndex, len(usersPredictions))
        rows = userPredictions[0].split('\n')
        userPredictions = {}
        for row in rows[1:]:
            #print(row)
            user, item, score = [field for field in row.split(' ') if field != ''][1:]
            user, item, score = int(user), int(item), float(score)
            #print(user, item, score)
            userPredictions[item] = score
        userScores = usersScores[user]
        manual_mrr = 0
        for rank, item in enumerate(userPredictions):
            # userScores[item] == 1
            if round(userPredictions[item]) == userScores[item]:# and userScores[item] == 1:
                manual_mrr = 1 / (rank + 1)
                break
        #print(mrr)
        #if mrr == 0:
        #    print('ho')
        mrrs += [manual_mrr]
        y_true = [[userScores[item] for item in userPredictions]]
        y_pred = [[userPredictions[item] for item in userPredictions]]
        evaluated_ndcgs += [ndcg(y_true, y_pred).numpy()]
        evaluated_mrrs += [mrr(y_true, y_pred).numpy()]
        evaluated_maps += [map_(y_true, y_pred).numpy()]
        #print(evaluated_ndcg)

def getAverage(l):
    return sum(l) / len(l)

for evaluated_l in [evaluated_ndcgs, evaluated_mrrs, evaluated_maps]:
    print(getAverage(evaluated_l))

'''
top = None gives identical results as top = 5:

0.9375520077286531
0.9281828035930674
0.9022409015830737

'''

#averageMrr = getAverage(mrrs)
#print(averageMrr, 1 / averageMrr)

