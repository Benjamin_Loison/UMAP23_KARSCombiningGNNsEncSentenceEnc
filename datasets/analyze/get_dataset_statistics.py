#!/usr/bin/python3

import csv

dataset = 'yt_100_most_subscribed_fr'

users = set()
items = set()
ratings = []
numberOfRatingsPerUser = {}

with open(f'../{dataset}/generation/dataset.tsv') as tsvFile:
    tsvReader = csv.reader(tsvFile, delimiter='\t')
    rows = list(tsvReader)
    for row in rows:
        #print(row)
        user, item, rating = row
        users.add(user)
        items.add(item)
        ratings.append(rating)
        numberOfRatingsPerUser[user] = numberOfRatingsPerUser.get(user, 0) + 1

positive = sum(rating == '1' for rating in ratings) / len(ratings)
averageRatingsPerUser = len(ratings) / len(users)
averageRatingsPerItem = len(ratings) / len(items)
# Assume that this if the formula `Together is Better: Hybrid Recommendations` used.
sparcity = 1 - averageRatingsPerUser / len(items)

print(f'{len(users)=}')
print(f'{len(items)=}')
print(f'{len(ratings)=}')
print(f'{positive=}')
print(f'{sparcity=}')
print(f'{averageRatingsPerUser=}')
print(f'{averageRatingsPerItem=}')

# Unclear `Together is Better: Hybrid Recommendations` `Table 1: Statistics of the datasets` `Avg. ratings/user` `±`.
#print(max(numberOfRatingsPerUser.values()))
