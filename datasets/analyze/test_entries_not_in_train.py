#!/usr/bin/python3

import os
import csv

dataset = 'movielens'

os.chdir(dataset)

users = set()
items = set()

with open('train.tsv') as tsvFile:
    tsvReader = csv.reader(tsvFile, delimiter='\t')
    for row in tsvReader:
        user, item, _ = [int(field) for field in row]
        users.add(user)
        items.add(item)

with open('test.tsv') as tsvFile:
    tsvReader = csv.reader(tsvFile, delimiter='\t')
    for row in tsvReader:
        user, item, _ = [int(field) for field in row]
        if not user in users:
            print('user', user)
        if not item in items:
            print('item', item)
