#!/usr/bin/python3

import csv
import random
from tqdm import tqdm

dataset = 'yt_100_most_subscribed_fr'

# Keep in mind that `pykeen_test.tsv` is not related to `test.tsv`, `train.tsv` is splitted into `pykeen_train.tsv` and `pykeen_test.tsv`.

rows = []

folder = f'../{dataset}/generation/split/0'
with open(f'{folder}/train.tsv') as tsvFile:
    tsvReader = csv.reader(tsvFile, delimiter='\t')
    for row in tqdm(list(tsvReader)):
        #print(row)
        user, item, rating = row
        # I assume that `http://dbpedia.org/resource/Batman_Forever` and `itemITEM_ID` is fundamentally identical
        rows += [[f'user{user}', 'like' if rating == '1' else 'dislike', f'item{item}']]

random.shuffle(rows)

splitIndex = int(len(rows) * 0.8)

def writeRows(fileName, rows):
    with open(f'{folder}/{fileName}.tsv', 'w') as tsvFile:
        tsvWriter = csv.writer(tsvFile, delimiter='\t')
        tsvWriter.writerows(rows)

writeRows('pykeen_train', rows[:splitIndex])
writeRows('pykeen_test', rows[splitIndex:])

