import pickle
import csv
import numpy as np
from tqdm import tqdm

with open('movielens_CompGCN_k=384.pickle', 'rb') as picklefile:
    data = pickle.load(picklefile)

print(type(data)) # <class 'dict'>
print(len(data)) # 26472

firstElement = list(data.values())[0]

print(type(firstElement)) # <class 'numpy.ndarray'>
print(len(firstElement)) # 384
print(firstElement.dtype) # float64

data = {}

with open('embeddings.tsv') as fd:
    rd = csv.reader(fd, delimiter='\t')
    for rowIndex, row in enumerate(tqdm(list(rd))):
        rowParsed = [float(element) for element in row]
        npRowParsed = np.array(rowParsed)
        data[rowIndex] = npRowParsed

with open('yt_100_most_subscribed_fr_CompGCN_k=384.pickle', 'wb') as picklefixe:
    pickle.dump(data, picklefixe)
