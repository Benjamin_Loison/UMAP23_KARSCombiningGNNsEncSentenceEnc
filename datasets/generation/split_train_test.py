#!/usr/bin/python3

# https://fr.wikipedia.org/w/index.php?title=Validation_crois%C3%A9e&oldid=208388770#Validation_crois%C3%A9e_d'un_contre_tous
import os
import csv
import random
from os import path
import itertools

dataset = 'yt_100_most_subscribed_fr'
splitFolder = 'split'

os.chdir(f'../{dataset}/generation')

ratings = {
    0: [],
    1: []
}

with open('dataset.tsv') as tsvFile:
    tsvReader = csv.reader(tsvFile, delimiter='\t')
    for row in tsvReader:
        user, item, rating = [int(field) for field in row]
        ratings[rating] += [[user, item]]

def getNumberOfRating(rating):
    return len(ratings[rating])

# For 80% - 20% training and testing sets.
testDatasetRatio = 0.2
likes = getNumberOfRating(1)
dislikes = getNumberOfRating(0)
#likesOverDislikesRatio = likes / dislikes
#print(likesOverDislikesRatio)

# Sanity check.
#print(likes / (likes + dislikes))

# Hope that the shuffle does not lead to issues due to ids not being 0, 1, ...

#print(ratings[1][:5])

for rating in ratings:
    random.shuffle(ratings[rating])

# Sanity check.
#print(ratings[1][:5])

def splitList(list_, wantedParts=1):
    length = len(list_)
    return [ list_[partIndex*length // wantedParts: (partIndex+1)*length // wantedParts]
             for partIndex in range(wantedParts) ]

# Assume that `partsNumber` is an integer.
partsNumber = int(1 / testDatasetRatio)
parts = {rating: splitList(ratings[rating], partsNumber) for rating in ratings}

# Pay attention to consider the whole arrays even if spliting evenly would result in fractions of ratings.
for splitIndex in range(partsNumber):
    print(splitIndex)
    splitIndexFolder = f'{splitFolder}/{splitIndex}'

    if not path.exists(splitIndexFolder):
        os.makedirs(splitIndexFolder)

    # Unclear how to easily put code in common.
    #for subset, [['train', 'test'], []]
    with open(f'{splitIndexFolder}/test.tsv', 'w') as tsvFile:
        tsvWriter = csv.writer(tsvFile, delimiter='\t')
        for rating in ratings:
            for part in parts[rating][-1]:
                tsvWriter.writerow(part + [rating])

    with open(f'{splitIndexFolder}/train.tsv', 'w') as tsvFile:
        tsvWriter = csv.writer(tsvFile, delimiter='\t')
        for rating in ratings:
            for part in list(itertools.chain(*parts[rating][:-1])):
                tsvWriter.writerow(part + [rating])
