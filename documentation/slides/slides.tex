\documentclass{beamer}

% Make bibliography.
\usepackage[style=alphabetic]{biblatex}
% For `\includepdf`.
\usepackage{pdfpages}
% For `\includesvg`.
\usepackage{svg}
% For `\subfigure`.
\usepackage{subcaption}
% For `\begin{tabularx}`.
\usepackage{tabularx}

% To avoid `PDF inclusion: multiple pdfs with page group included in a single page` warning.
\pdfsuppresswarningpagegroup=1

% To have PDF viewer table of contents.
\DeclareDocumentCommand \beginMyFrame { o m } {%
  \IfNoValueTF {#1} {%
    \section{#2}\begin{frame}{#2}%
  }{%
    \section{#2}\begin{frame}[#1]{#2}%
  }%
}

% Add bibliography references file.
\addbibresource{../references.bib}

% Only show at bottom right the current page number over the total number of pages.
\setbeamertemplate{navigation symbols}
{
    \usebeamerfont{footline}
    \usebeamercolor[fg]{footline}
    \hspace{1em}
    \insertframenumber/17%\inserttotalframenumber
    % TODO: replace \inserttotalframenumber to not count appendexes
}

\title{Hybrid graph and textual embeddings for YouTube video recommendations}
\date{January 25, 2024}
\author{Benjamin Loison, supervised by Fabien Tarissan}

% see report comments

\begin{document}

    % Missing a section.
    \frame{\titlepage}

    \beginMyFrame{Context}

        \begin{itemize}
            \item Recommendations to propose the most appropriate information to the user
            \pause
            % 2013 source...
            \item \href{https://www.mckinsey.com/industries/retail/our-insights/how-retailers-can-keep-up-with-consumers}{35\% of Amazon revenues are generated from its recommendation system}
        \end{itemize}

    \end{frame}

    \beginMyFrame{Main approaches}

      To generate recommendations the main approaches are:

      % Maybe illustration would be nice.
      % Could ensure same order graph and text or the reverse every time.
      \begin{itemize}
        \item a content-based approach
        \item a wisdom of the people approach, but is requires users to specify, explicitly or implicitly, their preferences
      \end{itemize}

      \pause

      The considered hybrid approach combines both approaches as it respectively use textual and graph embeddings.

    \end{frame}

    % Instead introduce work and when introduce an idea quote its paper.
    %\beginMyFrame{State of the art}

    %    \begin{itemize}
    %      \item Together is Better: Hybrid Recommendations Combining Graph Embeddings and Contextualized Word Representations, 2021~\cite{10.1145/3460231.3474272}.
    %      \item Combining Graph Neural Networks and Sentence Encoders for Knowledge-aware Recommendations, 2023~\cite{10.1145/3565472.3592965}.
    %    \end{itemize}

    %\end{frame}

    \beginMyFrame{Hybrid recommendation system}

    The hybrid recommendation system introduced in the article \cite{10.1145/3460231.3474272} leverages both graph and textual feature representations of user and item entities.

    \pause

    At a high-level my goal was:
    \begin{itemize}
      \item to reproduce this article results on their example dataset that is MovieLens. MovieLens is a collection of ratings (likes and dislikes) of users concerning movies.
      \item to establish a YouTube dataset similar to MovieLens
      \item apply the hybrid approach to my YouTube dataset
    \end{itemize}

    % Maybe directly consider the YouTube case? But then do not detail on DBBook and MovieLens which I worked hard to reproduce results.

    \pause

    Recommendations generation:

    \begin{enumerate}
      % or *entity feature* but less clear in my opinion
      \item \only<4->{users and items graph and textual embedding generation with state-of-the-art methods}
      \item \only<5->{feed these embeddings to a neural network learning hybrid representations}
      \item \only<6->{exploit these hybrid representations to generate recommendations}
    \end{enumerate}

  \end{frame}

    \beginMyFrame{MovieLens embeddings}

      % ```py
      % import pickle
      %
      % with open('yt_100_most_subscribed_fr_all-MiniLM-L12-v2.pickle', 'rb') as picklefile:
      %   data = pickle.load(picklefile)
      %
      % print([round(el, 3) for el in data[0]])
      % ```

      \begin{itemize}
          \item for items being movies:
              \begin{itemize}
                  \item each movie textual embedding is generated from its plot. For instance: \emph{hello everyone I hope you are well\dots}: [0.02, -0.025, \dots, -0.016]
                  \item each movie graph embedding is generated from relations between this movie and other entities. For instance: \emph{Titanic} $\xrightarrow{\text{starring}}$ \emph{Leonardo DiCaprio}: [0.02, -0.04, \dots, -0.009]%a given movie is in relation with its producer, its director and so on.
              \end{itemize}
          % reviewer wording is debatable but it allows a transition to user likes so let us keep it.
          \item for users being reviewers:
              \begin{itemize}
                  \item each user textual embedding is generated as the average of the textual embeddings of movies this user liked.
                  \item each user graph embedding is generated from its movie ratings. For instance, a given user liked a given movie.
              \end{itemize}
      \end{itemize}

      \pause

      \setlength\tabcolsep{0pt}
      \begin{table}
      \resizebox{\textwidth}{!}{
            \begin{tabular}{ | c | c | c | c | c | c | c | c | c | }
                \hline
                & \textbf{Users} & \textbf{Items} & \textbf{Ratings} & \textbf{\%Positive} & \textbf{Sparsity} & \textbf{Avg. ratings/user} & \textbf{Avg. ratings/item} & \textbf{Content feature}\\
                \hline
                MovieLens & 6,036 & 3,192 & 946,772 & 57.22\% & 95.09\% & 156.85 & 296.60 & plot\\
                \hline
            \end{tabular}
        }
        \caption{Statistics of MovieLens dataset.}
      \end{table}

    \end{frame}

    \beginMyFrame{Neural network configurations}

      \begin{figure}
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includesvg[inkscapelatex=false, width=\linewidth]{../figures/basic_deep_architecture}
            \caption{Single feature}
        \end{subfigure}
        \hfill
        \pause
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includesvg[inkscapelatex=false, width=\linewidth]{../figures/hybrid_feature_based_deep_architecture}
            \caption{Hybrid: feature-based concatenation}
        \end{subfigure}
      \end{figure}

    \end{frame}

    \beginMyFrame{Neural network configurations}

      \begin{figure}
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includesvg[inkscapelatex=false, width=\linewidth]{../figures/hybrid_feature_based_deep_architecture}
            \caption{Hybrid: feature-based concatenation}
        \end{subfigure}
        \hfill
        \pause
        \begin{subfigure}{0.49\textwidth}
            \centering
            \includesvg[inkscapelatex=false, width=\linewidth]{../figures/hybrid_entity_based_deep_architecture}
            \caption{Hybrid: entity-based concatenation}
        \end{subfigure}
      \end{figure}

    \end{frame}

    \beginMyFrame{Reproduction issues}

      The only implementation \cite{swapUniba_UMAP23_KARSCombiningGNNsEncSentenceEnc} related to the considered article \cite{10.1145/3460231.3474272}, containing embeddings and their generating tools, was \href{https://codeberg.org/Benjamin_Loison/UMAP23_KARSCombiningGNNsEncSentenceEnc/issues/35}{significantly broken, incomplete and outdated}. I substantially contributed to have it working~\cite{Benjamin_Loison_UMAP23_KARSCombiningGNNsEncSentenceEnc}
      % could precise DBBook, attention and dropout, hybrid gain not noticed before switching to performances

    \end{frame}

    % Prediction system does not sound good, hence use recommendation system and to simplify wording talk about recommendations and not predictions? Predictions sound very neural network result which is interesting.
    \beginMyFrame{Evaluate recommendations}

      % Could give an example?
      %\begin{figure}
        % How to avoid such hardcoding?
      %  \includesvg[inkscapelatex=false, height=0.6\textheight]{../figures/precision_recall}
      %  \caption{Precision and recall definitions.}
      %\end{figure}

      The metrics are defined as follows:
      \begin{itemize}
          \item $\text{precision} = \frac{\text{correctly predicted likes}}{\text{predicted likes}}$
          \item $\text{recall} = \frac{\text{correctly predicted likes}}{\text{actual likes}}$
          \pause
          \item $\text{F1 score} = \frac{2}{\text{recall}^{-1} + \text{precision}^{-1}}$
      \end{itemize}

      As a result, the F1 score symmetrically represents both precision and recall in a single metric.

      %\pause

      %$\text{F1 score} = \frac{2}{\text{recall}^{-1} + \text{precision}^{-1}}$ symmetrically represents both precision and recall in a single metric.

      %\pause
      %I also considered ranking being more precise however due to a lack of time I could not generate some comparable with the articles.

    \end{frame}

    \beginMyFrame{Bridge the performance gap}

      \begin{figure}
        % How to put factor in common?
        % How to avoid repeating `\includesvg`?
        % How to avoid having multiple SVGs?
        \only<1>{
          \includesvg[inkscapelatex=false, height=0.9\textheight]{../figures/own_embeddings_newest_text_embeddings_generator_0}
        }
        \only<2>{
          \includesvg[inkscapelatex=false, height=0.9\textheight]{../figures/own_embeddings_newest_text_embeddings_generator_1}
        }
        %\only<3>{
        %  \includesvg[inkscapelatex=false, height=0.9\textheight]{../figures/own_embeddings_newest_text_embeddings_generator_2}
        %}
        %\only<4>{
        %  \includesvg[inkscapelatex=false, height=0.9\textheight]{../figures/own_embeddings_newest_text_embeddings_generator_3}
        %}
        %\caption{Precision and recall definitions.}
      \end{figure}

    \end{frame}

    \beginMyFrame{Cross-validation}

      \begin{block}{One-shot evaluation:}

        \begin{itemize}
          \item Split randomly the dataset%:

          % Precising the ratio is debatable but justifies 5 splits, so let us keep it for the moment.
          %\begin{itemize}
          %  \item 80 \% for training
          %  \item 20 \% for testing
          %\end{itemize}

          \item Train the neural network with this split and evaluate its performances.
        \end{itemize}

      \end{block}

      \pause

      \begin{block}{Leave-one-out cross-validation:}

        To make result statistics robust, I implemented the following:

        \begin{itemize}

          \item Split randomly the dataset in 5 different ways.% keeping the 80 \% - 20 \% training and testing proportions.% Also paying attention have same proportion of likes and dislikes in each split.

          \item Train the neural network with these 5 splits and evaluate its performances.

        \end{itemize}

      \end{block}

    \end{frame}

    \beginMyFrame{Bridge the performance gap}

      \begin{figure}
        \includesvg[inkscapelatex=false, height=0.9\textheight]{../figures/own_embeddings_cross_validated}
        %\caption{Precision and recall definitions.}
      \end{figure}

    \end{frame}

    \beginMyFrame{YouTube dataset elaboration}

      \begin{itemize}
       \item considered the 100 most subscribed French YouTube channels
       \item as user ratings are private, I considered equivalently to MovieLens that someone liked a video if he left a comment on it. To have a balanced dataset I considered that each time a user comments a video, he has not commented a random video
      \end{itemize}

      As a result the neural network task consists in predicting among some videos, which ones a given user has probably the most commented.

      \setlength\tabcolsep{0pt}
      \begin{table}
      \resizebox{\textwidth}{!}{
            \begin{tabular}{ | c | c | c | c | c | c | c | c | c | }
                \hline
                & \textbf{Users} & \textbf{Items} & \textbf{Ratings} & \textbf{\%Positive} & \textbf{Sparsity} & \textbf{Avg. ratings/user} & \textbf{Avg. ratings/item} & \textbf{Content feature}\\
                \hline
                MovieLens & 6,036 & 3,192 & 946,772 & 57.22\% & 95.09\% & 156.85 & 296.60 & plot\\
                \hline
                YouTube & 448,214 & 48,826 & 1,814,862 & 50.00\% & 99.99\% & 4.05 & 37.16 & captions\\
                \hline
            \end{tabular}
        }
        \caption{Statistics of the datasets.}
        \label{table:Statistics of the datasets}
      \end{table}

    \end{frame}

    \beginMyFrame{Known sources of bias}

      \begin{itemize}
        % In average is quite appropriate as it is about 5 splits and even with in from a probabilistic point of view it is justified.
        \item In average 20 \% of the graph testing set was part of the training set due to a lack of time.
        \item Random not commenting policy may not be appropriate for recommendations.
      \end{itemize}

    \end{frame}

    \beginMyFrame{Results on YouTube dataset}

      \begin{figure}
        \includesvg[inkscapelatex=false, height=0.8\textheight]{../figures/youtube}
      \end{figure}

    \end{frame}

    \beginMyFrame{Results on YouTube dataset without the graph configuration}

      \begin{figure}
        \includesvg[inkscapelatex=false, height=0.6\textheight]{../figures/youtube_without_graph}
      \end{figure}

      %Paid attention to traceability.

    \end{frame}

    %\beginMyFrame{Conclusion}



    %\end{frame}

    \beginMyFrame{Meta-information}

      % Could complete
      \begin{itemize}
        \item Leverage YouTube data for an artificial intelligence project
        \item Hands-on experience with GPUs thanks to OVH free trial
      \end{itemize}

    \end{frame}

    \beginMyFrame[allowframebreaks]{References}

        % How to only have above section table of contents entry in PDF viewer but not one generated from below?
        \printbibliography

    \end{frame}

    % Could have appendix

    %\section{Report}
    % How to keep links working?
    % How to have increasing page numbers?
    %\includepdf[pages=-]{report.pdf}

\end{document}
